# 2.5 Deliverable

### 1. Add the address of your GitLab repository to your answer sheet file. Please make sure the repositoryis public and do not delete it until the end of the semester.
https://gitlab.com/381group31/Lab6

### 2. Make a screenshot from the History tab of your Git client application. It must at least show two commits to your GitLab repository in this step. Add the screenshot to your answer sheet file.
(see 2.5.2_screenshot.png in repo)

### 3. Write down your reason for selecting your preferred Git client.
Very Simple UI design, very easy to use. And it's what we usually use for our daily github activities