# 3.6 Deliverable
### 1. Add the URL of your ‘students’ service to your answer sheet file.
https://65df9285ff5e305f32a28f4a.mockapi.io/students

### 2. Add a snapshot of the list of students to your answer sheet file (similar to Figure 7, but a complete list).
(see 3.6.2_screenshot.png in repo)

### 3. Add a snapshot of the courses of your second student to your answer sheet file (similar to Figure 8, the first course must be ‘ENSF381’ in the picture).
(see 3.6.3_screenshot.png in repo)